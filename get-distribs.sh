#!/bin/bash

# GET ISO FILES
# VERSIONS AUTO UPDATE:
# - DEBIAN
# TO PUT IN .ENV:
# - LMDE CINNAMON
# - DEBIAN LIVE
# - SHRED OS
# - UBUNTU

# LMDE-LIVE-64 - OK
cd /tmp
url="https://mirror.johnnybegood.fr/mint-cd/debian/lmde-6-cinnamon-64bit.iso"
wget $url
sudo mount -o loop lmde-6-cinnamon-64bit.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/lmde-live-64
sudo umount /mnt
sudo rm -rf /tmp/lmde-6-cinnamon-64bit.iso


# LMDE-LIVE-32 - OK
url="https://mirror.johnnybegood.fr/mint-cd/debian/lmde-6-cinnamon-32bit.iso"
wget $url
sudo mount -o loop ./lmde-6-cinnamon-32bit.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/lmde-live-32
sudo umount /mnt
sudo rm -rf /tmp/lmde-6-cinnamon-32bit.iso

# DEBIAN-64 - OK
url="https://ftp.debian.org/debian/dists/bookworm/main/installer-amd64/current/images/netboot/netboot.tar.gz"
wget $url
mkdir netboot
tar -xzvf netboot.tar.gz -C netboot
sudo cp -r netboot/. /var/lib/tftpboot/debian-12/64/
sudo rm -rf netboot netboot.tar.gz

# DEBIAN-32(netboot) - OK
url="https://ftp.debian.org/debian/dists/bookworm/main/installer-i386/current/images/netboot/netboot.tar.gz"
wget $url
mkdir netboot
tar -xzvf netboot.tar.gz -C netboot
sudo cp -r netboot/. /var/lib/tftpboot/debian-12/32/
sudo rm -rf netboot netboot.tar.gz


# DEBIAN-LIVE-64 - OK
url="https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.2.0-amd64-gnome.iso"
wget $url
sudo mount -o loop ./debian-live-12.2.0-amd64-gnome.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/debian-live-64/
sudo umount /mnt
sudo rm -rf /tmp/debian-live-12.2.0-amd64-gnome.iso

# DEBIAN-LIVE-32
url="https://cdimage.debian.org/debian-cd/current-live/i386/iso-hybrid/debian-live-12.2.0-i386-gnome.iso"
wget $url
sudo mount -o loop ./debian-live-12.2.0-i386-gnome.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/debian-live-32/
sudo umount /mnt
sudo rm -rf /tmp/debian-live-12.2.0-i386-gnome.iso

# KALI-LIVE-64 - OK
url="https://cdimage.kali.org/kali-2023.3/kali-linux-2023.3-live-amd64.iso"
wget $url
sudo mount -o loop ./kali-linux-2023.3-live-amd64.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/kali-live-64
sudo umount /mnt
sudo rm -rf /tmp/kali-linux-2023.3-live-amd64.iso

# SHREDOS-64
url="https://github.com/PartialVolume/shredos.x86_64/releases/download/v2021.08.2_21_x86-64_0.32.023/shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso"
wget $url
sudo mount -o loop ./shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/shredos-64
sudo umount /mnt
sudo rm -rf /tmp/shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso

# SHREDOS-32
url="https://github.com/PartialVolume/shredos.x86_64/releases/download/v2021.08.2_21_x86-64_0.32.023/shredos-2021.08.2_21_i586_0.32.023_20220126.iso"
wget $url
sudo mount -o loop ./shredos-2021.08.2_21_i586_0.32.023_20220126.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/shredos-32
sudo umount /mnt
sudo rm -rf /tmp/shredos-2021.08.2_21_i586_0.32.023_20220126.iso

# GPARTED-LIVE-64 - OK
url="https://downloads.sourceforge.net/gparted/gparted-live-1.4.0-1-amd64.iso"
wget $url
sudo mount -o loop ./gparted-live-1.4.0-1-amd64.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/gparted-live-64
sudo umount /mnt
sudo rm -rf /tmp/gparted-live-1.4.0-1-amd64.iso

# GPARTED-LIVE-32 - OK
url="https://downloads.sourceforge.net/gparted/gparted-live-1.4.0-1-i686.iso"
wget $url
sudo mount -o loop ./gparted-live-1.4.0-1-i686.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/gparted-live-32
sudo umount /mnt
sudo rm -rf /tmp/gparted-live-1.4.0-1-i686.iso

# UBUNTU-LIVE - OK 
url="https://releases.ubuntu.com/22.04/ubuntu-22.04.3-desktop-amd64.iso"
wget $url
sudo mount -o loop ./ubuntu-22.04.3-desktop-amd64.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/ubuntu-live
sudo umount /mnt
sudo rm -rf /tmp/ubuntu-22.04.3-desktop-amd64.iso
