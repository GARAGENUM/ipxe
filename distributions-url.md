# IPXE

Urls pour les distributions suivantes (07-2023):

- DEBIAN:
    - 32 bits
    - 64 bits
    - Lives (64):
        - Gnome
        - Cinnamon
        - KDE
        - Mate
- UBUNTU 24:
    - 64 bits
    - Live (64)
- KALI:
    - 32 bits
    - 64 bits
    - Live (64)
- SHRED OS:
    - 32 bits
    - 64 bits
- LMDE:
    - Live (32)
    - Live (64)


## DEBIAN

### 32-BITS

```bash
wget http://debian.proxad.net/debian/dists/stable/main/installer-i386/current/images/netboot/debian-installer/i386/initrd.gz
wget https://ftp.debian.org/debian/dists/stable/main/installer-i386/current/images/netboot/debian-installer/i386/inux
wget https://ftp.debian.org/debian/dists/stable/main/installer-i386/current/images/netboot/debian-installer/i386/firmware.cpio.gz
```

### 64-BITS

```bash
wget https://ftp.debian.org/debian/dists/stable/main/installer-amd64/current/images/netboot/debian-installer/amd64/linux
wget https://ftp.debian.org/debian/dists/stable/main/installer-amd64/current/images/netboot/debian-installer/amd64/initrd.gz
wget http://debian.proxad.net/debian/dists/stable/main/installer-amd64/current/images/netboot/debian-installer/amd64/firmware.cpio.gz
```

### LIVES

#### GNOME

```bash
wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.0.0-amd64-gnome.iso
```

#### CINNAMON

```bash
wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.0.0-amd64-cinnamon.iso
```

#### KDE

```bash
wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.0.0-amd64-kde.iso
```

#### MATE

```bash
wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.0.0-amd64-mate.iso
```

## UBUNTU 24

### 64-BITS

```bash
wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/ubuntu-installer/amd64/linux
wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/ubuntu-installer/amd64/initrd.gz
```

### LIVE

```bash
wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/xen/vmlinuz
wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/xen/initrd.gz
```

## KALI

### ISO

```bash
wget https://cdimage.kali.org/current/kali-linux-2023.2a-installer-amd64.iso
```

### 32-BITS

```bash
wget https://http.kali.org/kali/dists/kali-rolling/main/installer-i386/current/images/netboot/debian-installer/i386/linux
wget https://http.kali.org/kali/dists/kali-rolling/main/installer-i386/current/images/netboot/debian-installer/i386/initrd.gz
```

### 64-BITS

```bash
wget https://http.kali.org/kali/dists/kali-rolling/main/installer-amd64/current/images/netboot/debian-installer/amd64/linux
wget https://http.kali.org/kali/dists/kali-rolling/main/installer-amd64/current/images/netboot/debian-installer/amd64/initrd.gz
```

### LIVE

```bash
wget https://cdimage.kali.org/current/kali-linux-2023.2-live-amd64.iso
```

## SHRED OS

### SHREDOS-32

```bash
wget https://github.com/PartialVolume/shredos.x86_64/releases/download/v2021.08.2_21_x86-64_0.32.023/shredos-2021.08.2_21_i586_0.32.023_20220126.iso

sudo mount -o loop ./shredos-2021.08.2_21_i586_0.32.023_20220126.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/shredos-32
sudo umount /mnt
sudo rm -rf /tmp/shredos-2021.08.2_21_i586_0.32.023_20220126.iso
```

### SHREDOS-64

```bash
wget https://github.com/PartialVolume/shredos.x86_64/releases/download/v2021.08.2_21_x86-64_0.32.023/shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso

sudo mount -o loop ./shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/shredos-64
sudo umount /mnt
sudo rm -rf /tmp/shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso
```

## LMDE

### LMDE-LIVE-32

```bash
wget https://ftp.crifo.org/mint-cd/debian/lmde-5-cinnamon-32bit.iso

sudo mount -o loop ./lmde-5-cinnamon-32bit.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/lmde-live-32
sudo umount /mnt
sudo rm -rf /tmp/lmde-5-cinnamon-32bit.iso
```

### LMDE-LIVE-64

```bash
wget https://ftp.crifo.org/mint-cd/debian/lmde-5-cinnamon-64bit.iso

sudo mount -o loop lmde-5-cinnamon-64bit.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/lmde-live-64
sudo umount /mnt
sudo rm -rf /tmp/lmde-5-cinnamon-64bit.iso
```