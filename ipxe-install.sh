#!/bin/bash

# VARIABLES
IP=$(ip route get 1.2.3.4 | awk '{print $7}')
INTERFACE="$(ip addr show | awk '/inet.*brd/{print $NF; exit}')"

# DEPENDANCES
sudo apt update -y
sudo apt install wget -y
#sudo apt install wimboot mkisofs cabextract -y

# DHCP SERVICE
sudo apt install isc-dhcp-server -y

mv /etc/default/isc-dhcp-server /etc/default/isc-dhcp-server.BAK
sed -i "s/NETINTERFACE/$INTERFACE/" ./configs/isc-dhcp-server; sudo cp ./configs/isc-dhcp-server /etc/default/isc-dhcp-server
mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.BAK
sed -i "s/IP/$IP/g" ./configs/dhcpd.conf; sudo cp ./configs/dhcpd.conf /etc/dhcp/

# TFTP
sudo apt install tftpd-hpa -y
sudo mkdir -p /var/lib/tftpboot
sed -i "s/IP/$IP/" ./configs/tftpd-hpa; cp ./configs/tftpd-hpa /etc/default/tftpd-hpa

# NFS
sudo apt install nfs-kernel-server -y

# LAMP
sudo apt install apache2 php libapache2-mod-php php-mysql php-curl php-gd php-intl php-json php-mbstring php-xml php-zip -y

ln -s /var/lib/tftpboot /var/www/html/tftpboot

cd /var/lib/tftpboot
sudo wget http://boot.ipxe.org/undionly.kpxe
sudo wget http://boot.ipxe.org/ipxe.efi

cd -
# CHAIN.IPXE
#cd /tmp
#git clone git://git.ipxe.org/ipxe.git
#cd /tmp/ipxe/src
#sed -i "s/IP/$IP/" ./configs/chain.ipxe; sudo cp ./configs/chain.ipxe /tmp/ipxe/src/chain.ipxe

# INSTALL.IPXE (MENU)
sudo sed -i "s/IP/$IP/" ./configs/install.ipxe; sudo cp ./configs/install.ipxe /var/www/html/install.ipxe
sudo ln -s /var/www/html/install.ipxe /var/lib/tftpboot/

# ALLOW SHARING IN THE NFS CONF FILE :
sudo echo "/var/lib/tftpboot/ *(async,no_root_squash,no_subtree_check,ro) /etc/exports"

# SAMBA FOR WIN INSTALLS
sudo apt install samba -y
sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.BAK
sudo cp ./configs/smb.conf /etc/samba/smb.conf
sudo systemctl enable smbd

# CREATION DES DOSSIERS DISTRIBS
cd /var/lib/tftpboot
sudo mkdir -p debian-12/64 debian-12/32 debian-live-32 debian-live-64 lmde-live-32 lmde-live-64 ubuntu-live kali-live-32 kali-live-64 shredos-32 shredos-64 gparted-live-32 gparted-live-64 windows 
cd -

# INSTALL DES DISTRIBS (SCRIPT MAX)
sudo ./get-distribs.sh

# WINDOWS PE -- TODO
# INSTALL DEPENDANCES
# PLACEMENT FICHIERS WINPE + AUTRES WINDOWS
# PRE CONFIG WINPE POUR AUTOBOOT (NETUSE DANS STARTCMD)

# COPIE PRESSED FOR DEBIAN 12
sudo cp ./configs/debian-12-preseed.cfg /var/lib/tftpboot/debian-12/64/

# GOOD RIGHTS (voir pour windows...)
sudo chown -R root:root /var/lib/tftpboot
sudo chmod 755 /var/lib/tftpboot

# REDEMARRAGE DES SERVICES
sudo service tftpd-hpa restart
sudo service isc-dhcp-server restart
sudo systemctl restart nfs-kernel-server
sudo systemctl restart smbd

# REBOOT
echo "redémarrage!"
sudo reboot
