# IPXE 

Ce projet a pour but de déployer un serveur IPXE proposant des distributions Linux open source

## TABLE DES MATIERES

[INSTALLATION (ON DEBIAN)](#installation-(on-debian))

[CONFIGURATION FILES](#configuration-files)

[STEPS SCRIPT INSTALL](#steps-script-install)
- [Variable](#variables)
- [Dependencies](#dependencies)
- [DHCP Server](#dhco-server)
- [TFTP Server](#tftp-server)
- [NFS Server](#nsf-server)
- [LAMP Server](#lamp-server)
- [Prepare iPXE startup files](#prepare-ipxe-startup-files)
- [SAMBA for win installs](#samba-for-win-installs)
- [Directories for distributions](#directories-for-distributions)
- [TFTPD-HPA Restart](#tftpd-hpa-restart)
- [Install Distributions](#install-distributions)
- [In progress](#in-progress)
- [Good rights](#good-rights)
- [Reboot](#variables)

[MANUAL INSTALLATION OF DISTRIBUTIONS](#manual-installation-of-distributions)

## INSTALLATION (ON DEBIAN)

```bash
sudo ./ipxe-install.sh
```

## CONFIGURATION FILES

**IMPORTANT : THE CONFIGURATION FILES HAVE BEEN EDITED UPSTREAM AND REPLACE THE DEFAULT FILES**

More about : https://git.legaragenumerique.fr/GARAGENUM/ipxe/get-distribs.sh

### [DHCP](#dhcp-server)

- ```./configs/isc-dhcp-server /etc/default/isc-dhcp-server```
- ```./configs/dhcpd.conf /etc/dhcp/```

### [TFTP](#tftp-server)

- ```./configs/tftpd-hpa /etc/default/tftpd-hpa```

### [MENU](#ipxe-menu)

- ```./configs/install.ipxe /var/www/html/install.ipxe```

### [SAMBA](#samba-for-win-installs)

- ```./configs/smb.conf /etc/samba/smb.conf```

## STEPS SCRIPT INSTALL

about : [ipxe-install.sh](https://gitlab.com/garagenum/greg/pxe/-/blob/ipxe/ipxe-install.sh)

### Variables

- Check server ip
- Network interface check

### Dependencies

- Update the system
- Install wget program
- Install : ```wimboot``` ```mkisofs``` ```cabextract```

### DHCP Server

Installation of the DHCP server on the server.
(WARNING : A server must have a fixed ip address ! )
- Install  isc-dhcp-server
- Edit dhcp.conf located in ```etc/dhcp/``` >>> adapt to our current network

For our configuration we have chosen as address range between ```.150``` & ```.199```

### TFTP Server

The TFTP server is the one that will provide the necessary files for network startup. 
- Install TFTP server
- Edit tftpd-hpa file located in ```/etc/default/```
- Replace the TFTP address with the address of our ixpe server
- Then restart the service : isc-dhcp-server

### NFS Server

The NFS server will be necessary in some cases, it allows to leave files available to the started system. 
- Install nfs-kernel-server

### LAMP Server

To work, iPXE needs an HTTP server. 
- Install HTTP server
- Create a symbolic link from tftpboot to the LAMP server

### Prepare iPXE startup files

- In this directory : ```/var/lib/tftpboot```
- Downloading files from the Web : 
```undionly.kpxe``` and ```ipxe.efi```

### IPXE Menu

- The iPXE menu must be located on the web server, located in ```/var/www/html/```
- Create a symbolic link from ```/var/www/html/install.ipxe``` to ```/var/lib/tftpboot/```
- Allow sharing in the NFS configuration file :

```bash
sudo echo "/var/lib/tftpboot/ *(async,no_root_squash,no_subtree_check,ro) /etc/exports"
```

- Then restart the service nfs-kernel-server

### SAMBA for win installs

- Install Samba
- Enable smbd service
- Then restart smbd service

### Directories for distributions

- In this directory : ```/var/lib/tftpboot```
- Create folders for all used distributions

### TFTPD-HPA Restart

- Restart tftpd-hpa service

### Install Distributions

- Call the script ```get-distribs.sh```

### In progress

- WINDOWS PE -- TODO
- INSTALL DEPENDANCES
- PLACEMENT FICHIERS WINPE + AUTRES WINDOWS
- PRE CONFIG WINPE POUR AUTOBOOT (NETUSE DANS STARTCMD)

### Good rights

- Re-establishing the right rights to root : ```/var/lib/tftpboot```
- Modification of the rights of a directory and its sub-directories : ```/var/lib/tftpboot```

### Reboot

- Reboot and and test the operation

## DONE !

## MANUAL INSTALLATION OF DISTRIBUTIONS

about : [get-distribs.sh](https://git.legaragenumerique.fr/GARAGENUM/ipxe/get-distrib.sh)

### Process

- In this directory ```/tmp```
- We have added a variable url, in which is referenced the url that will serve as a source to download the iso
- Then we mount iso file in this directory ```/mnt```

Explication : The loop option of mount (it's not a type, it's an option) tells mount that this is a "looping" device - a normal file to use as a block device.  In the background, mount sets up a loopback device, then mounts it to /mnt in our case.
- We copy everything in ```/mnt``` to a folder in the ```/var/lib/tftpboot/``` directory where our installation images are located
- Then we dismantle everything in ```/mnt```
- Finally we delete the iso present in the /mnt directory

## TO DO

- [ ] distribs versions in .env
- [ ] maj get-distrib.sh with good url and netboot files
- [ ] maj ipxe-install.sh prompt ip range
- [ ] preseed debian 12 64 pour post install garage full auto
